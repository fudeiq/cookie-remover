var urls = [];
var domains = [];
var delay = 60;
var interval;
var running = false;

function saveData() {
  localStorage.setItem("urls", JSON.stringify(urls));
  localStorage.setItem("domains", JSON.stringify(domains));
  localStorage.setItem("delay", delay);
}

function loadData() {
  if (localStorage.getItem("urls")) {
    urls = JSON.parse(localStorage.getItem("urls"));
  }
  if (localStorage.getItem("domains")) {
    domains = JSON.parse(localStorage.getItem("domains"));
  }
  if (localStorage.getItem("delay")) {
    delay = localStorage.getItem("delay");
  }
}

function start() {
  interval = setInterval(deleteCookies, delay * 1000);
}

function stop() {
  clearInterval(interval);
}

function buildUrl(domain, path, secure) {
  if (domain.substr(0, 1) === '.')
    domain = domain.substring(1);
  return "http" + ((secure) ? "s" : "") + "://" + domain + path;
}

function deleteAllCookies() {
  chrome.cookies.getAll({}, cookie => {
    cookie.forEach(c => {
      if (c.domain) {
        chrome.cookies.remove({
          url: buildUrl(c.domain, c.path, c.secure),
          name: c.name
        });
      }
    })
  })
}

function deleteCookies() {
  if (domains.length > 0) {
    for (let i = 0; i < domains.length; i++) {
      chrome.cookies.getAll({
          domain: domains[i]
        },
        cookie => {
          cookie.forEach(c => {
            chrome.cookies.remove({
              url: urls[i],
              name: c.name
            });
          });
        }
      );
    }
  } else {
    deleteAllCookies();
  }
}
loadData();