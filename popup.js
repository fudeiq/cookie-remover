var root = document.getElementById('root');
var add_button = document.getElementById('add');
var start_button = document.getElementById('start');
var stop_button = document.getElementById('stop');
var setDelay_button = document.getElementById('setDelay');
var input = document.getElementById('delay');
var pages_list = document.getElementById('pages');
var manual = document.getElementById('manual');
var reset = document.getElementById('reset');

var background = chrome.extension.getBackgroundPage();

function getDomain(url) {
  server = url.match(/:\/\/(.[^/:#?]+)/)[1];
  parts = server.split(".");
  domain = parts[parts.length - 2] + "." + parts[parts.length - 1];
  return domain;
}

var current_domain = "";
var current_url = "";

// set current url and domain 
chrome.tabs.query({
  'active': true,
  'lastFocusedWindow': true
}, function (tabs) {
  current_url = tabs[0].url;
  current_domain = getDomain(current_url);
});

input.value = background.delay;

// set interval delay
setDelay_button.addEventListener('click', function () {
  background.delay = parseInt(input.value);
  background.running = false;
  setStatus();
  background.saveData();
});

// push current url and domain to background script
add_button.addEventListener('click', function () {
  background.urls.push(current_url);
  background.domains.push(current_domain);
  updateList();
  background.saveData();
});

// start interval 
start_button.addEventListener('click', function () {
  background.deleteCookies();
  background.running = true;
  setStatus();
  background.start();
});

// stop interval 
stop_button.addEventListener('click', function () {
  background.stop();
  background.running = false;
  setStatus();
});

// manual delete
manual.addEventListener('click', function () {
  background.deleteCookies();
});

// reset everything
reset.addEventListener('click', function () {
  background.stop();
  background.urls = [];
  background.domains = [];
  background.delay = 60;
  input.value = background.delay;
  background.running = false;
  background.saveData();
  setStatus();
  updateList();
});

function setStatus() {
  if (background.running) {
    document.getElementById('status').innerHTML = "Status: running";
  } else {
    document.getElementById('status').innerHTML = "Status: idle";
  }
}

function updateList() {
  pages_list.innerHTML = "";
  for (let i = 0; i < background.domains.length; i++) {
    let text = document.createElement('div');
    text.style.cursor = "pointer";
    text.innerHTML = background.domains[i];
    text.onclick = function () {
      background.urls.splice(i, 1);
      background.domains.splice(i, 1);
      updateList();
    }
    pages_list.appendChild(text);
  }
}
updateList();
setStatus();